FROM cloudron/base:0.10.0
MAINTAINER Seafile Developers <support@cloudron.io>

ENV VERSION="6.2.2"

RUN mkdir -p /app/code/
WORKDIR /app/code/

RUN apt-get update && \
    apt-get install -y python2.7 libpython2.7 python-setuptools python-imaging python-ldap python-urllib3 ffmpeg python-pip python-mysqldb python-memcache && \
    rm -r /var/cache/apt /var/lib/apt/lists

RUN curl -L https://download.seadrive.org/seafile-server_${VERSION}_x86-64.tar.gz | tar -xz --strip-components 1 -f -

RUN ln -s /app/data/conf /app/conf && \
    ln -s /app/data/ccnet /app/ccnet && \
    ln -s /app/data/seahub-data /app/seahub-data && \
    ln -s /run/logs /app/logs && \
    ln -s /run/pids /app/pids && \
    mv /app/code/seahub/media/avatars /app/code/seahub/media/avatars_orig && ln -s /app/seahub-data/avatars /app/code/seahub/media/avatars && \
    mv /app/code/runtime /app/code/runtime_orig && ln -s /run/seahub /app/code/runtime

ADD start.sh nginx.conf setup-seafile-mysql.py /app/code/

CMD ["/app/code/start.sh"]
