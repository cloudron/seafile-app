#!/bin/bash

set -eu -o pipefail

mysql="mysql --user=${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} --host=${MYSQL_HOST}"

CCNET_DB="${MYSQL_DATABASE_PREFIX}ccnet"
SEAFILE_DB="${MYSQL_DATABASE_PREFIX}seafile"
SEAHUB_DB="${MYSQL_DATABASE_PREFIX}seahub"

# has to be prior to first run code
echo "=> Ensure runtime folders"
mkdir -p /run/logs /run/pids /run/seahub

echo "=> Copy seahub config"
cp /app/code/runtime_orig/* /run/seahub/

# first run
if ! [ -e  /app/data/conf ]; then
    echo "=> First run"

    echo "=> Create databases"
    $mysql -e "create database ${CCNET_DB} character set = 'utf8';"
    $mysql -e "create database ${SEAFILE_DB} character set = 'utf8';"
    $mysql -e "create database ${SEAHUB_DB} character set = 'utf8';"

    echo "=> Create initial folders"
    mkdir -p /app/data/conf /app/data/seahub-data
    mkdir -p /app/data/seahub-data/avatars

    echo "=> Setup seafile"
    ./setup-seafile-mysql.sh auto \
        --server-name Seafile \
        --server-ip ${APP_ORIGIN} \
        --seafile-dir /app/data/data \
        --use-existing-db 1 \
        --mysql-host ${MYSQL_HOST} \
        --mysql-port ${MYSQL_PORT} \
        --mysql-user ${MYSQL_USERNAME} \
        --mysql-user-passwd ${MYSQL_PASSWORD} \
        --ccnet-db ${CCNET_DB} \
        --seafile-db ${SEAFILE_DB} \
        --seahub-db ${SEAHUB_DB}

    echo "=> Create temporary admin credentials file for setup"
    echo "{\"email\": \"admin@cloudron.io\", \"password\": \"changeme\" }" > /app/conf/admin.txt
fi

echo "=> Copy built in avatars"
cp -rf /app/code/seahub/media/avatars_orig/* /app/data/seahub-data/avatars/

echo "=> Start seafile"
./seafile.sh start

echo "=> Start seahub"
./seahub.sh start-fastcgi

echo "=> Start nginx"
nginx -c /app/code/nginx.conf &

tail -F /run/logs/ccnet.log /run/logs/controller.log /run/logs/seafile.log /run/logs/seahub.log /run/logs/seahub_django_request.log
